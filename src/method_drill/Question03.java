package method_drill;

public class Question03 {

	static void printRandomMessage() {
		int n = (int)(3 * Math.random());

		if(n == 1) {
			System.out.println("おはよう ");
		}else if(n == 2) {
			System.out.println("こんにちは ");
		}else {
			System.out.println("こんばんは ");
		}
	}
	public static void main(String[] args) {
		printRandomMessage();
	}
}
