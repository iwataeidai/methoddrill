package method_drill;

public class Question19 {
	static int getMinValue(int[] array) {

		int min = array[0];

		for(int i = 0; i < array.length; i++) {
			if(array[i] <  min) {
				min = array[i];
			}
		}
		return min;
	}
	public static void main(String[] args) {
		int i[] = {5,7,1,6,8,4,2,3,5,4};
		System.out.println(getMinValue(i));
	}
}
