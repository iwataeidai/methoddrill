package method_drill;

public class Question14 {
	static double getAbsoluteValue(double value) {
		double d = Math.abs(value);
		return d;
	}
	public static void main(String[] args) {
		System.out.println(getAbsoluteValue(-2.4));
	}
}
