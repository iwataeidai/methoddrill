package method_drill;

public class Question12 {
	static double getSquareRootOf2() {
		double d = Math.sqrt(2.0);
		return d;
	}
	public static void main(String[] args) {
		System.out.println(getSquareRootOf2());
	}
}
