package method_drill;

public class Question21 {
	static String getLongestString(String[] array) {
		String maxName = "";
		int max = array[0].length();
		for (int i = 0; i<array.length; i++) {
			if(array[i].length() >= max) {
				max = array[i].length();
				maxName = array[i];
			}
		}
		return maxName;
	}
	public static void main(String[] args) {
		String i[] = {"あ", "こんにちは", "よろしくおねがいします", "岩田"};
		System.out.println(getLongestString(i));
	}
}
