package method_drill;

public class Question06 {

	static void printRandomMessage(String name) {

		int n = (int)(3 * Math.random());
//		System.out.println(n);

		if (n == 1){
			System.out.println("おはよう" + name + "さん");
		}else if(n == 2) {
			System.out.println("こんばんは" + name + "さん");
		}else {
			System.out.println("こんにちは" + name + "さん");
		}
	}
	public static void main (String[]args) {
		printRandomMessage("岩田");
	}
}
