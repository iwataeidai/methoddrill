package method_drill;

public class Question22 {
	static double getDistanceFromOrigin(Point p) {
		double length = Math.sqrt(p.x*p.x+p.y*p.y);
		return length;
	}
	public static void main(String[] args) {
		Point p = new Point(5.5, 6.2);
		System.out.println(getDistanceFromOrigin(p));
	}
}
