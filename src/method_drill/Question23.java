package method_drill;

public class Question23 {
	static double getDistanceBetweenTwoPoints(Point p0, Point p1) {
		double answear = Math.sqrt( (p0.x - p1.x)*(p0.x -p1.x)+(p0.y - p1.y)*(p0.y - p1.y));
		return answear;
	}
	public static void main(String[] args) {

		Point pa = new Point(5.5, 6.2);
		Point pb = new Point(3.2, 7.2);

		System.out.println(pa.x);
		System.out.println(getDistanceBetweenTwoPoints(pa ,pb));
	}
}
