package method_drill;

public class Question11 {

	static String getWeatherForecast(){

		int i = (int)(3*Math.random());
		String day = "";
		if (i == 1){
			day =  "今日";
		}else if(i == 2) {
			day =  "明日";
		}else {
			day =  "明後日";
		}

		int n = (int)(3*Math.random());
		String weather = "";
		if (n == 1){
			weather = "晴れ";
		}else if(n == 2) {
			weather = "雨";
		}else {
			weather = "曇り";
		}
		return day + "の天気は" + weather;
	}
	public static void main(String[] args) {
		System.out.println(getWeatherForecast());
	}
}
