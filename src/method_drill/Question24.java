package method_drill;

public class Question24 {
	static Point getBarycenter(Point[] points){

		double sumX = 0;
		double sumY = 0;

		for(int i = 0; i < points.length; i++) {
			sumX += points[i].x;
			sumY += points[i].y;
		}

		Point answear = new Point (0.0,0.0);
		answear.x = sumX / points.length;
		answear.y = sumY / points.length;

		return answear ;
	}
public static void main(String[] args) {

	Point[] points = {
					new Point(5.5, 6.2),
					new Point(3.2, 7.2),
					new Point(4.8, 2.5)
	};
	System.out.println(getBarycenter(points).x + "," + getBarycenter(points).y);
}
}
