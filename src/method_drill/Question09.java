package method_drill;

public class Question09 {
	static void printMaxValue(double a, double b, double c) {
		System.out.println(Math.max(a,Math.max(b, c)));
	}
	public static void main(String[] args) {
		printMaxValue(3.5,6.8,9.7);
	}
}
