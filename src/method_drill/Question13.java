package method_drill;

public class Question13 {
	static String getRandomMessage(String name) {

		int i = (int)(3*Math.random());

		if (i == 1){
			return "こんばんは" + name + "さん";
		}else if(i == 2) {
			return "こんにちは" + name + "さん";
		}else {
			return "おはよう" + name + "さん";
		}
	}
	public static void main(String[] args) {
		System.out.println(getRandomMessage("岩田"));
	}
}
