package method_drill;

public class Question20 {
	static double getAverage(double[] array) {
		double sum = 0;
		for(int i =0; i<array.length; i++ ) {
			sum += array[i];
		}
		double average = sum / array.length;
		return average;
	}
	public static void main(String[] args) {
		double i[] = {5.4, 2.8, 6.5, 7.8};
		System.out.println(getAverage(i));
	}
}
