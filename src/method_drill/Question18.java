package method_drill;

public class Question18 {
	static String getMessage(String name, boolean isKid) {
		if(isKid == true) {
			return "こんにちは" + name + "ちゃん";
		}else {
			return "こんにちは" + name + "さん";
		}
	}
	public static void main(String[] args) {
		System.out.println(getMessage("岩田", false));
	}
}
